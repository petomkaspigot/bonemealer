package me.petomka.bonemealer;

import lombok.Getter;
import me.petomka.bonemealer.commands.BonemealerCommand;
import me.petomka.bonemealer.commands.BonemealerreloadCommand;
import me.petomka.bonemealer.configuration.ConfigDefault;
import me.petomka.bonemealer.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	@Getter
	private static Main instance;

	@Getter
	private boolean worldEditHooked = false;

	@Override
	public void onEnable() {
		instance = this;

		for (Message message : Message.values()) {
			message.addDefault();
		}
		for (ConfigDefault configDefault : ConfigDefault.values()) {
			configDefault.addDefault();
		}
		getConfig().options().copyDefaults(true);
		saveConfig();

		getCommand("bonemealer").setExecutor(new BonemealerCommand());
		getCommand("bonemealerreload").setExecutor(new BonemealerreloadCommand());

		if (Bukkit.getPluginManager().getPlugin("WorldEdit") != null) {
			worldEditHooked = true;
		}
	}

}
