package me.petomka.bonemealer.messages;

import com.google.common.base.Preconditions;
import me.petomka.bonemealer.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public enum Message {

	NO_PERMISSION("no permission", ChatColor.RED + "You don't have permission to do that."),
	NOT_A_PLAYER("not a player", ChatColor.RED + "You have to be a player to do that."),
	USAGE("usage", ChatColor.RED + "Usage: /{alias} <radiusXZ> [<radiusY>] [<skipChance>] {<flag>} OR /{alias} #we [<skipChance>] {<flag>}"),
	ILLEGAL_ARGUMENT("illegal argument", ChatColor.RED + "Invalid command input: {input}"),
	GREATER_THAN_RADIUS("greater than allowed radius", ChatColor.RED + "Given radius in {dir} direction is greater than the allowed maximum ({maximum})"),
	ILLEGAL_COORDINATE("illegal coordinate", ChatColor.RED + "Your Y-coordinate is illegal: {y}"),
	BONEMEAL_APPLIED("bone meal applied", ChatColor.GREEN + "Applied bone meal in radius {radius} successfully!"),
	CONFIG_RELOADED("config reloaded", ChatColor.GREEN + "Config was reloaded!"),
	NO_WORLDEDIT_PLUGIN("no worldedit plugin", ChatColor.RED + "WorldEdit could not be found!"),
	NO_WORLDEDIT_SESSION("no worldedit session", ChatColor.RED + "No WorldEdit session could be found."),
	NO_WORLDEDIT_SELECTION("no worldedit selection", ChatColor.RED + "Please make a WorldEdit selection first!"),
	BONEMEAL_APPLIED_WORLDEDIT("bone meal applied worldedit", ChatColor.GREEN + "Applied bone meal to your WorldEdit selection successfully!");

	private final String key;
	private final String defaultValue;

	Message(String key, String defaultValue) {
		this.key = "message." + key;
		this.defaultValue = defaultValue;
	}

	public String get(Object... placeholderAndReplacements) {
		Preconditions.checkArgument(placeholderAndReplacements.length % 2 == 0);
		String result = Main.getInstance().getConfig().getString(key, defaultValue);
		for(int a = 0; a < placeholderAndReplacements.length; a+=2) {
			Preconditions.checkNotNull(placeholderAndReplacements[a], "Placeholder");
			Preconditions.checkNotNull(placeholderAndReplacements[a+1], "Placerholder replacement");

			result = result.replace("{" + placeholderAndReplacements[a].toString() + "}", placeholderAndReplacements[a+1].toString());
		}
		return ChatColor.translateAlternateColorCodes('&', result);
	}

	public void addDefault() {
		Main.getInstance().getConfig().addDefault(key, untranslateAlternateColorCodes('&', defaultValue));
	}

	public void send(CommandSender sender, Object... placeholderAndReplacements) {
		sender.sendMessage(this.get(placeholderAndReplacements));
	}

	public static String untranslateAlternateColorCodes(char altColorChar, String textToTranslate) {
		char[] b = textToTranslate.toCharArray();
		for(int i = 0; i < b.length - 1; ++i) {
			if (b[i] == ChatColor.COLOR_CHAR) {
				b[i] = altColorChar;
				b[i + 1] = Character.toLowerCase(b[i + 1]);
			}
		}
		return new String(b);
	}

}
