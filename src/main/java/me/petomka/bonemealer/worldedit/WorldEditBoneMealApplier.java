package me.petomka.bonemealer.worldedit;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import me.petomka.bonemealer.messages.Message;
import me.petomka.bonemealer.model.BoneMealApplier;
import me.petomka.bonemealer.model.DefaultBoneMealApplier;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collection;

public class WorldEditBoneMealApplier implements BoneMealApplier {

	private WorldEditPlugin worldEditPlugin;

	public WorldEditBoneMealApplier() {
		worldEditPlugin = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
	}

	@Override
	public void applyBoneMeal(Player player, double skipChance, int x1, int y1, int z1, int x2, int y2, int z2, Collection<Material> unwanted) {
		LocalSession session = worldEditPlugin.getSession(player);
		if (session == null) {
			Message.NO_WORLDEDIT_SESSION.send(player);
			return;
		}

		Region selection;
		try {
			selection = session.getSelection(worldEditPlugin.wrapPlayer(player).getWorld());
		} catch (IncompleteRegionException ignored) {
			Message.NO_WORLDEDIT_SELECTION.send(player);
			return;
		}

		BlockVector3 min = selection.getMinimumPoint();
		BlockVector3 max = selection.getMaximumPoint();

		DefaultBoneMealApplier.applyBoneMeal(DefaultBoneMealApplier.DEFAULT_BLOCK_PREDICATE.
						and(block -> selection.contains(BlockVector3.at(block.getX(), block.getY(), block.getZ()))),
				player, skipChance, min.getX(), min.getY(), min.getZ(), max.getX(), max.getY(), max.getZ(), unwanted);
		Message.BONEMEAL_APPLIED_WORLDEDIT.send(player);
	}
}
