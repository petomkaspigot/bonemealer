package me.petomka.bonemealer.configuration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.petomka.bonemealer.Main;

import java.util.logging.Level;

@RequiredArgsConstructor
@Getter
public enum ConfigDefault {

	MAX_RADIUS("max xz radius", 25),
	MAX_Y_RADIUS("max y radius", 10),
	DEFAULT_Y_RADIUS("default y radius", 10),
	PERMISSION("permission", "bonemealer.use"),
	PERMISSION_RELOAD("permission to reload", "bonemealer.reload");

	private final String key;
	private final Object defaultValue;

	public void addDefault() {
		Main.getInstance().getConfig().addDefault(key, defaultValue);
	}

	public int getInt() {
		Object o = Main.getInstance().getConfig().get(key, defaultValue);
		if (o instanceof Integer) {
			return (int) o;
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Illegal config value for key \"" + key + "\"");
		}
		if (defaultValue instanceof Integer) {
			return (int) defaultValue;
		}
		throw new RuntimeException("This is not an integer");
	}

	public String getString() {
		Object o = Main.getInstance().getConfig().get(key, defaultValue);
		if (o instanceof String) {
			return (String) o;
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Illegal config value for key \"" + key + "\"");
		}
		if (defaultValue instanceof String) {
			return (String) defaultValue;
		}
		throw new RuntimeException("This is not a string");
	}

}
