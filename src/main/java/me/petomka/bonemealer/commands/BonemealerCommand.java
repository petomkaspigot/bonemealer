package me.petomka.bonemealer.commands;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import me.petomka.bonemealer.Main;
import me.petomka.bonemealer.configuration.ConfigDefault;
import me.petomka.bonemealer.messages.Message;
import me.petomka.bonemealer.model.BoneMealApplierHandler;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BonemealerCommand implements TabExecutor {

	private final Map<String, Boolean> altBooleanValues = ImmutableMap.of(
			"yes", Boolean.TRUE,
			"no", Boolean.FALSE,
			"1", Boolean.TRUE,
			"0", Boolean.FALSE
	);

	private final Function<String[], List<Material>> flagArgCollector = args -> flags.entrySet()
			.stream()
			.filter(e -> ArrayUtils.contains(args, e.getKey()))
			.map(Map.Entry::getValue)
			.collect(Collectors.toList());

	private final BiFunction<String[], String, List<String>> flagArgFilter = (args, lastArg) -> flags.keySet()
			.stream()
			.filter(s -> s.toLowerCase().startsWith(lastArg))
			.filter(s -> !ArrayUtils.contains(args, s))
			.collect(Collectors.toList());

	private static <K, V> Map<K, V> mapOf(Object... keyValues) {
		Preconditions.checkArgument(keyValues.length % 2 == 0);
		Map<Object, Object> map = new HashMap<>();
		for (int a = 0; a < keyValues.length; a += 2) {
			map.put(keyValues[a], keyValues[a + 1]);
		}
		return (Map<K, V>) map;
	}

	public static final Map<String, Material> flags = mapOf(
			"tallGrass", Material.TALL_GRASS,
			"grass", Material.GRASS,
			"redFlowers", Material.POPPY,
			"yellowFlowers", Material.DANDELION,
			"oxeye", Material.OXEYE_DAISY,
			"azure", Material.AZURE_BLUET,
			"fern", Material.FERN,
			"tallFern", Material.LARGE_FERN,
			"seaGrass", Material.SEAGRASS,
			"tallSeaGrass", Material.TALL_SEAGRASS,
			"allium", Material.ALLIUM,
			"redTulip", Material.RED_TULIP,
			"orangeTulip", Material.ORANGE_TULIP,
			"whiteTulip", Material.WHITE_TULIP,
			"pinkTulip", Material.PINK_TULIP,
			"tubeCoral", Material.TUBE_CORAL,
			"tubeCoralFan", Material.TUBE_CORAL_FAN,
			"tubeCoralWallFan", Material.TUBE_CORAL_WALL_FAN,
			"brainCoral", Material.BRAIN_CORAL,
			"brainCoralFan", Material.BRAIN_CORAL_FAN,
			"brainCoralWallFan", Material.BRAIN_CORAL_WALL_FAN,
			"bubbleCoral", Material.BUBBLE_CORAL,
			"bubbleCoralFan", Material.BUBBLE_CORAL_FAN,
			"bubbleCoralWallFan", Material.BUBBLE_CORAL_WALL_FAN,
			"fireCoral", Material.FIRE_CORAL,
			"fireCoralFan", Material.FIRE_CORAL_FAN,
			"fireCoralWallFan", Material.FIRE_CORAL_WALL_FAN,
			"hornCoral", Material.HORN_CORAL,
			"hornCoralFan", Material.HORN_CORAL_FAN,
			"hornCoralWallFan", Material.HORN_CORAL_WALL_FAN
	);

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {

		if (!(commandSender instanceof Player)) {
			Message.NOT_A_PLAYER.send(commandSender);
			return true;
		}

		if (!commandSender.hasPermission(ConfigDefault.PERMISSION.getString())) {
			Message.NO_PERMISSION.send(commandSender);
			return true;
		}

		if (args.length == 0) {
			Message.USAGE.send(commandSender, "alias", label);
			return true;
		}

		Player player = (Player) commandSender;

		if(args[0].equalsIgnoreCase("#we")) {
			onWorldEdit(player, args);
			return true;
		}

		onDefault(player, args);
		return true;
	}

	private void onDefault(Player player, String[] args) {
		int radiusXZ;
		try {
			radiusXZ = Integer.parseInt(args[0]);
			if (radiusXZ < 0) {
				throw new IllegalArgumentException("invalid input");
			}
		} catch (Exception e) {
			Message.ILLEGAL_ARGUMENT.send(player, "input", args[0]);
			return;
		}

		int maxRadius = ConfigDefault.MAX_RADIUS.getInt();
		if (radiusXZ > maxRadius) {
			Message.GREATER_THAN_RADIUS.send(player, "maximum", maxRadius, "dir", "xz");
			return;
		}

		int radiusY = ConfigDefault.DEFAULT_Y_RADIUS.getInt();
		if (args.length >= 2) {
			try {
				radiusY = Integer.parseInt(args[1]);
			} catch (Exception e) {
				Message.ILLEGAL_ARGUMENT.send(player, "input", args[1]);
			}
		}

		if (radiusY > ConfigDefault.MAX_Y_RADIUS.getInt()) {
			Message.GREATER_THAN_RADIUS.send(player, "maximum", ConfigDefault.MAX_Y_RADIUS.getInt(), "dir", "y");
			return;
		}

		double skipChance = 0;
		if (args.length >= 3) {
			try {
				skipChance = Double.parseDouble(args[2]);
			} catch (Exception e) {
				Message.ILLEGAL_ARGUMENT.send(player, "input", args[2]);
				return;
			}
		}
		int yCoord = player.getLocation().getBlockY() - 1;

		if (yCoord < 0) {
			Message.ILLEGAL_COORDINATE.send(player, "y", yCoord);
			return;
		}

		int xCoord = player.getLocation().getBlockX(), zCoord = player.getLocation().getBlockZ();

		BoneMealApplierHandler.applyDefault(player, skipChance, xCoord - radiusXZ, yCoord - radiusY,
				zCoord - radiusXZ, xCoord + radiusXZ, yCoord + radiusY, zCoord + radiusXZ,
				flagArgCollector.apply(args));

		Message.BONEMEAL_APPLIED.send(player, "radius", radiusXZ);
	}

	private void onWorldEdit(Player player, String[] args) {
		double skipChance = 0;
		if(args.length >= 2) {
			try {
				skipChance = Double.parseDouble(args[1]);
			} catch (Exception e) {
				Message.ILLEGAL_ARGUMENT.send(player, "input", args[1]);
				return;
			}
		}

		if(!Main.getInstance().isWorldEditHooked()) {
			Message.NO_WORLDEDIT_PLUGIN.send(player);
			return;
		}

		BoneMealApplierHandler.applyWorldEdit(player, skipChance, 0, 0, 0, 0, 0, 0,
				flagArgCollector.apply(args));
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String label, String[] args) {
		String lastArg = args[args.length - 1].toLowerCase();
		if (args.length == 1 && "#we".startsWith(lastArg)) {
			return ImmutableList.of("#we");
		}
		if(args[0].equalsIgnoreCase("#we") && args.length >= 3) {
			return flagArgFilter.apply(args, lastArg);
		}
		if (args.length >= 4 && args.length <= 3 + flags.size()) {
			return flagArgFilter.apply(args, lastArg);
		}
		return Collections.emptyList();
	}

}
