package me.petomka.bonemealer.commands;

import me.petomka.bonemealer.Main;
import me.petomka.bonemealer.configuration.ConfigDefault;
import me.petomka.bonemealer.messages.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BonemealerreloadCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
		if(!commandSender.hasPermission(ConfigDefault.PERMISSION_RELOAD.getString())) {
			Message.NO_PERMISSION.send(commandSender);
			return true;
		}
		Main.getInstance().reloadConfig();
		Message.CONFIG_RELOADED.send(commandSender);
		return true;
	}

}
