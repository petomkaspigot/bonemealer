package me.petomka.bonemealer.model;

import me.petomka.bonemealer.Main;
import me.petomka.bonemealer.worldedit.WorldEditBoneMealApplier;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collection;

public class BoneMealApplierHandler {

	private static DefaultBoneMealApplier defaultBoneMealApplier = new DefaultBoneMealApplier();

	private static BoneMealApplier worldEditBoneMealApplier = null;

	public static void applyDefault(Player player, double skipChance, int x1, int y1, int z1, int x2, int y2, int z2, Collection<Material> unwanted) {
		defaultBoneMealApplier.applyBoneMeal(player, skipChance, x1, y1, z1, x2, y2, z2, unwanted);
	}

	public static void applyWorldEdit(Player player, double skipChance, int x1, int y1, int z1, int x2, int y2, int z2, Collection<Material> unwanted) {
		if(worldEditBoneMealApplier == null && Main.getInstance().isWorldEditHooked()) {
			worldEditBoneMealApplier = new WorldEditBoneMealApplier();
		}
		if(worldEditBoneMealApplier == null && !Main.getInstance().isWorldEditHooked()) {
			return;
		}
		worldEditBoneMealApplier.applyBoneMeal(player, skipChance, x1, y1, z1, x2, y2, z2, unwanted);
	}

}
