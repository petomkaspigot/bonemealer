package me.petomka.bonemealer.model;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collection;

public interface BoneMealApplier {

	void applyBoneMeal(Player player, double skipChance, int x1, int y1, int z1, int x2, int y2, int z2, Collection<Material> unwanted);

}
