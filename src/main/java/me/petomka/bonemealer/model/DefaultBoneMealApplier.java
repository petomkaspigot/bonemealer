package me.petomka.bonemealer.model;

import me.petomka.bonemealer.commands.BonemealerCommand;
import me.petomka.bonemealer.reflection.ReflectionUtil;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;

public class DefaultBoneMealApplier implements BoneMealApplier {

	public static final Predicate<Block> DEFAULT_BLOCK_PREDICATE = block -> !BonemealerCommand.flags.containsValue(block.getType());

	private static final ItemStack boneMeal = new ItemStack(Material.BONE_MEAL);
	private static final Object nmsBoneMeal = ReflectionUtil.itemStackAsNmsCopy(boneMeal);

	public static void applyBoneMeal(Predicate<Block> shouldBoneMeal, Player player, double skipChance, int x1, int y1, int z1, int x2, int y2, int z2, Collection<Material> unwanted) {

		World world = player.getWorld();
		Object nmsWorld = ReflectionUtil.craftWorldGetHandle(world);
		Random random = ThreadLocalRandom.current();

		for (int x = x1; x <= x2; x += random.nextInt(1) + 4) {
			for (int z = z1; z <= z2; z += random.nextInt(1) + 4) {
				for (int y = y1; y <= y2; y++) {

					if (y < 0) {
						continue;
					}

					if (!shouldBoneMeal.test(world.getBlockAt(x, y, z))) {
						continue;
					}

					if (random.nextDouble() <= skipChance) {
						continue;
					}

					Object nmsBlockPosition = ReflectionUtil.constructBlockPosition(x, y, z);
					ReflectionUtil.applyBoneMeal(nmsBoneMeal, nmsWorld, nmsBlockPosition);

				}
			}
		}

		for (int x = x1 - 5; x <= x2 + 5; x++) {
			for (int z = z1 - 5; z <= z2 + 5; z++) {
				for (int y = y1 - 1; y <= y2 + 1; y++) {

					if(y < 0) {
						continue;
					}

					Block block = world.getBlockAt(x, y, z);

					for (Material unwantedMaterial : unwanted) {

						if (block.getType() == unwantedMaterial) {
							block.setType(Material.AIR);
						}

					}

				}
			}
		}
	}

	@Override
	public void applyBoneMeal(Player player, double skipChance, int x1, int y1, int z1, int x2, int y2, int z2, Collection<Material> unwanted) {
		DefaultBoneMealApplier.applyBoneMeal(DEFAULT_BLOCK_PREDICATE, player, skipChance, x1, y1, z1, x2, y2, z2, unwanted);
	}

}
